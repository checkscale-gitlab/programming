# Local IspellDict: de
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2018-2021 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+KEYWORDS: JavaScript, Some-Origin Policy, Cross-Origin Sharing,

In der Regel befolgen Browser die sogenannte
[[https://en.wikipedia.org/wiki/Same-origin_policy][Same-Origin-Policy]],
nach der beispielsweise JavaScript von einer Web-Seite nur dann auf
das DOM einer zweiten Web-Seite zugreifen darf, wenn beide Web-Seiten
denselben Ursprung (engl. /origin/, deutsch manchmal auch irreführend
Domäne) aufweisen, ihre URLs also (1) dasselbe Schema ({{{zb}}} beide
~http~ oder beide ~https~), (2) denselben Servernamen (IP-Adresse oder
Domänenname, {{{zb}}} ~www.uni-muenster.de~) und (3) denselben Port
({{{zb}}} Port 8080) aufweisen.  Durch diese Einschränkung der
Same-Origin-Policy soll verhindert werden, dass schützenswerte Inhalte
einer Web-Seite ({{{zb}}} Cookies) von einer anderen (bösartigen)
Web-Seite ausgelesen oder verändert werden.

Um Ressourcen aus verschiedenen Domänen zu kombinieren und die
Zusammenarbeit zwischen verschiedenen Ursprüngen gezielt zuzulassen,
gibt es einerseits den Ansatz
[[https://en.wikipedia.org/wiki/Cross-origin_resource_sharing][Cross-Origin Resource Sharing (CORS)]].
Andererseits stellt die JavaScript-Methode
[[https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage][Window.postMessage()]]
ein Beispiel für die sichere Cross-Origin-Kommunikation zwischen
Window-Objekten ({{{zb}}} Seite und Pop-up oder iframe) dar, das
[[http://blog.teamtreehouse.com/cross-domain-messaging-with-postmessage][in diesem Blog-Beitrag]]
aufgegriffen wird.  Unter anderem verweist der Blog-Beitrag auf eine
[[http://codepen.io/matt-west/pen/lpExI][Live-Demo auf CodePen]].
Bearbeiten Sie zu dieser Demo bitte folgende Aufgaben!

1. Die Live-Demo funktioniert bereits länger nicht mehr.  Welches
   offensichtliche Problem besteht?  Wenn Sie den Quelltext ansehen,
   erkennen Sie vielleicht auch ein subtileres Problem, das die Demo
   bereits im letzten Jahr hat scheitern lassen.
2. In Learnweb finden Sie ein ZIP-Archiv mit auf der Demo basierendem
   Code.  Vergewissern Sie sich, dass Sie den Code verstehen und
   testen ihn dann, indem Sie mit Docker zwei Web-Server (auf
   unterschiedlichen Ports, die Sie dem Quelltext entnehmen) nutzen.
   Geben Sie an, wie Sie die Server starten und was Sie zum Test
   im Browser gemacht haben.

# Local Variables:
# indent-tabs-mode: nil
# End:
