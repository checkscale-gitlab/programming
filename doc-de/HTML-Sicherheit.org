# Local IspellDict: de
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2018-2020 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+KEYWORDS: OWASP, Cross-Site Scripting, XSS,

* Sicherheit

Programmierung im Web-Kontext ist anfällig für diverse
Sicherheitslücken, wozu das
[[https://owasp.org/][Open Web Application Security Project (OWASP)]]
zahlreiche OER bereitstellt (vgl.
[[https://owasp.org/www-community/attacks/][„List of Attacks“]]).

Lesen Sie, was unter
[[https://owasp.org/www-community/Injection_Theory][Injection Theory]]
verstanden wird, in deren Rahmen Cross Site Scripting  (XSS)
([[https://owasp.org/www-community/attacks/xss/][Erläuterungen
bei OWASP]],
[[https://en.wikipedia.org/wiki/Cross-site_scripting][Wikipedia-Artikel]])
erklärt werden kann.  Zum Schutz vor XSS liefert
[[https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html][OWASP ein Cheat Sheet]].
Beachten Sie, dass beispielsweise die
[[https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage][Dokumentation zur Java-Script-Methode ~postMessage()~]]
folgenden Hinweis enthält: „always verify the syntax of the received message“

# Local Variables:
# indent-tabs-mode: nil
# End:
