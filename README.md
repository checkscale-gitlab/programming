<!--- Local IspellDict: en -->
<!--- SPDX-FileCopyrightText: 2019,2021 Jens Lechtenbörger -->
<!--- SPDX-License-Identifier: CC0-1.0 -->

Org source files in this project are not meant to be exported directly
but to be included in small wrapper files that add title slide, theme,
etc.  See [presentations on Communication and Collaboration Systems](https://gitlab.com/oer/oer-courses/cacs)
and [presentations on miscellaneous topics](https://gitlab.com/oer/misc)
for examples.

BibTeX files for citations in these Org files can be found in the
[literature project](https://gitlab.com/oer/literature).
