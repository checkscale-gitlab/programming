# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2020 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

# The slides below originally were part of that file:
# https://gitlab.com/oer/OS/-/blob/master/Operating-Systems-Security.org

* Passwords
  :PROPERTIES:
  :CUSTOM_ID: passwords
  :END:
  #+INDEX: Password (Security)

** Authentication with Passwords
    - *Randomness* of passwords
      - Measured via entropy
      - Ca. 4 bits per character in natural language
	- (Contrast this with 8 bits, which are necessary for typical
           representation)
      - Thus, 32 bits per 8 character password
	- Instead of 64 bits if characters were randomly distributed
	- 2^{32} is about a factor of 10^9 smaller than 2^{64}
    - Consequently, security of memorizable passwords is *limited*

** Password Advice
  - *Write down* strong and diverse passwords
    - Strong: Use entire keyboard alphabet, no real words, > 10
       characters
    - Where to store?
      - Martin Hellman: “[[https://www.technologyreview.com/s/405473/calling-cryptographers/][Write down your password. Your wallet is a lot more secure than your computer.]]”
      - Attackers?

** Password Checking
   - *Never* store passwords in *plain text*
   - Typical setup
     - System stores (salted -- next slide) *hash value* of password
       - GNU/Linux: ~/etc/shadow~
       - Windows: Security Accounts Manager (SAM) or Active Directory
          (AD)
   - Verification
     - System computes hash value for entered password string
     - Compare computed to stored value
   - (Alternative, e.g., Kerberos)
     - Use hash value of password as symmetric encryption key

** Sample Password Attacks
   - [[https://en.wikipedia.org/wiki/Social_engineering_(security)][Social engineering]]
   - Guessing
   - Observe or record
     - Shoulder surfing, (web)cam, keylogger, network sniffing, extract
	from RAM, deduce from cache, etc.
     - [[https://dl.acm.org/citation.cfm?doid=2897845.2897847][Friend or Foe?: Your Wearable Devices Reveal Your Personal PIN]]

** Brute Force and Salting
   - Brute force = Try “all” combinations
     - Various heuristics, implemented in tools
       - Try common patterns first
         - Databases of stolen passwords (e.g., “123456”)
         - Variations of dictionary words (e.g., “p@ssw0rd” above)
       - [[https://en.wikipedia.org/wiki/Rainbow_table][Rainbow tables]]: precomputed hash values
       - Counter measure: *Salting*
	 - Concatenate password with random number (=salt), then compute hash
	 - Store random number and hash
